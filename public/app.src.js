/* eslint-disable camelcase */
/* eslint-disable eqeqeq */
/**
 * onReady
 */
const apiLocation = 'https://recover-backend-naga-tlhetx5dvq-an.a.run.app/api';

var lang = "fil";
var messagesObject = {};

$(document).ready(() => {
  loadMessages();
  loadHotlines();
  // Sets initial route view
  router.handle();

  /**
   * hide splashscreen/preloader
   */
  $('.splashscreen').fadeOut();

  actions.travel.fetchCountries();

  // handle route change event
  window.addEventListener('hashchange', () => {
    router.handle();
  });

  fetch(`/assets/dataset/barangay.json`).then(p => p.json()).then(res => {
    res.forEach(r => {
      $(".barangayAbout").append(`<option value="${r['barangay_name']}">${r['barangay_name']}</option>`)
    });
  });
});

/**
 * Store Methods
 */
const store = {
  get(key) {
    return sessionStorage.getItem(key);
  },
  getObject(key) {
    return JSON.parse(sessionStorage.getItem(key));
  },
  set(key, value) {
    sessionStorage.setItem(key, value);
  },
  setObject(key, value) {
    sessionStorage.setItem(key, JSON.stringify(value));
  }
};

const utils = {
  array: {
    contains(needle, haystack) {
      if (haystack.indexOf(needle) !== -1) return true;
    }
  }
};

/**
 * Messages Methods
 */
const messages = {
  toast(message) {
    // @ts-ignore
    // eslint-disable-next-line no-undef
    M.toast({
      html: message
    });
  },
  form: {
    required: 'Kailangan sagutan ito',
    requiredAll: 'Kailangan sagutan lahat'
  },
  required: {
    fname : "",
    mname : "",
    lname : "",
    bday : "",
    status : "",
    gender : "",
    occupation : ""
  },
  travel: {
    country: 'Pumili ng Bansa',
    region: 'Pumili ng Rehiyon',
    city: 'Pumili ng Lungsod'
  },
  info : {
    loadingResult : ""
  }
};

/**
 * Router Methods
 */

const router = {
  clear() {
    $('section').hide();
  },
  navigate(id) {
    this.clear();
    if($(`#${id}Activity`).html() == undefined) {
      // Handle error 404
      $(`#notFoundActivity`).fadeIn();
    } else {
      $(`#${id}Activity`).fadeIn();
    }
    window.location.hash = `#${id}`;
    window.scrollTo(0, 0);


  },
  handle() {
    if (window.location.hash.substr(1)) {
      var hash = window.location.hash.substr(1);
      if (hash !== '!/') {
        // show route when not hashbang
        this.navigate(hash);
      } else {
        // show initial
        this.navigate('welcome');
      }
    } else {
      this.navigate('welcome');
    }
  }
};
/**
 * Form Methods
 */
const form = {
  getValue(id) {
    return $(`#${id}`).val();
  },
  radio: {
    getValue(name) {
      return $(`input[name='${name}']:checked`).val();
    },
    getAll(list) {
      var result = {};
      list.forEach(o => {
        var val = form.radio.getValue(o);
        result[o] = val;
      });
      return result;
    },
    getAllAsArray(list) {
      var result = [];
      list.forEach(o => {
        var val = form.radio.getValue(o);
        if (val) result.push(o);
      });
      return result;
    }
  },
  getAll(list) {
    var result = {};
    list.forEach(o => {
      var val = this.getValue(o);
      result[o] = val || null;
    });
    return result;
  }
};

// mobile
$('#countriesOneMobile').change(() => {
  $('#citiesOneMobile')
    .html(`<option disabled selected>${messages.travel.city}</option>`);

  var country = $('#countriesOneMobile').val()

  if (country === 'Philippines') {
    actions
    .travel
    .getRegions()
    .then(res => {
      res = res.sort();
      res = [...new Set(res)];
      res.forEach(r =>
        $('#regionsOneMobile').append(`<option value='${r.name}'>${r.long}</option>`));
    });

    $('#regionsOneMobile').parent().css({ display: 'block' })
  } else {
    $('#regionsOneMobile').parent().css({ display: 'none' })
  }

  let res = window['cities'][$('#countriesOneMobile').val()];
  res = res.sort();
  res = [...new Set(res)];
  res.forEach(r =>
    $('#citiesOneMobile').append(`<option value='${r}'>${r}</option>`));
});

$('#countriesTwoMobile').change(() => {
  $('#citiesTwoMobile')
    .html(`<option disabled selected>${messages.travel.city}</option>`);

  var country = $('#countriesTwoMobile').val()

  if (country === 'Philippines') {
    actions
    .travel
    .getRegions()
    .then(res => {
      res = res.sort();
      res = [...new Set(res)];
      res.forEach(r =>
        $('#regionsTwoMobile').append(`<option value='${r.name}'>${r.long}</option>`));
    });

    $('#regionsTwoMobile').parent().css({ display: 'block' })
  } else {
    $('#regionsTwoMobile').parent().css({ display: 'none' })
  }

  let res = window['cities'][$('#countriesTwoMobile').val()];
  res = res.sort();
  res = [...new Set(res)];
  res.forEach(r =>
    $('#citiesTwoMobile').append(`<option value='${r}'>${r}</option>`));
});

$('#addTravelOneMobile').click(() => {
  $('#travel2Mobile').fadeIn();
  $('#addTravelOneMobile').hide();
});

// desktop
$('#countriesOneDesktop').change(() => {
  $('#citiesOneDesktop')
    .html(`<option disabled selected>${messages.travel.city}</option>`);

  var country = $('#countriesOneDesktop').val()

  if (country === 'Philippines') {
    actions
    .travel
    .getRegions()
    .then(res => {
      res = res.sort();
      res = [...new Set(res)];
      res.forEach(r =>
        $('#regionsOneDesktop').append(`<option value='${r.name}'>${r.long}</option>`));
    });

    $('#regionsOneDesktop').parent().css({ display: 'block' })
  } else {
    $('#regionsOneDesktop').parent().css({ display: 'none' })
  }

  let res = window['cities'][$('#countriesOneDesktop').val()];
  res = res.sort();
  res = [...new Set(res)];
  res.forEach(r =>
    $('#citiesOneDesktop').append(`<option value='${r}'>${r}</option>`));
});

$('#countriesTwoDesktop').change(() => {
  $('#citiesTwoDesktop')
    .html(`<option disabled selected>${messages.travel.city}</option>`);

  var country = $('#countriesTwoDesktop').val()

  if (country === 'Philippines') {
    actions
    .travel
    .getRegions()
    .then(res => {
      res = res.sort();
      res = [...new Set(res)];
      res.forEach(r =>
        $('#regionsTwoDesktop').append(`<option value='${r.name}'>${r.long}</option>`));
    });

    $('#regionsTwoDesktop').parent().css({ display: 'block' })
  } else {
    $('#regionsTwoDesktop').parent().css({ display: 'none' })
  }

  let res = window['cities'][$('#countriesTwoDesktop').val()];
  res = res.sort();
  res = [...new Set(res)];
  res.forEach(r =>
    $('#citiesTwoDesktop').append(`<option value='${r}'>${r}</option>`));
});

$('#addTravelOneDesktop').click(() => {
  $('#travel2Desktop').fadeIn();
  $('#addTravelOneDesktop').hide();
});

/**
 * User defined actions
 */

const actions = {
  recommend() {
    messages.toast(messages.info.loadingResult);

    const symptoms = store.getObject('symptoms');
    const travelHistoryLocal = store.getObject('travelHistoryLocal');
    const travelHistoryInternational = store.getObject('travelHistoryInternational');
    const exposure = store.getObject('direct_contact');
    const clusterILI = store.getObject('clusterILI');
    const comorbidity = store.getObject('co-morbidity');
    const withElderly = store.getObject('with-elderly');
    const severeCondition = store.getObject('severe_condition');
    if (symptoms == [] || symptoms.length > 0) {
      if (severeCondition == 'true') symptoms.push('severe-condition');
    }

    var data = {
      data: {
        symptoms: symptoms,
        travelHistoryInternational: travelHistoryInternational || [],
        travelHistoryLocal: travelHistoryLocal || [],
        exposure: exposure,
        clusterILI: clusterILI,
        comorbitities: comorbidity,
        withElderly: withElderly
      }
    };

    const mildSymptomArray = [
      'fever',
      'dry-cough',
      'fatigue',
      'sputum',
      'sore-throat',
      'myalgia-arthralgia',
      'chills',
      'nausea',
      'vomiting',
      'nasal-congestion',
      'diarrhea'
    ];

    const severeSymptomArray = [
      'breathing-difficulty',
      'whistling-sound',
      'severe-condition'
    ];

    const isExposed = exposure ||
      (travelHistoryInternational && travelHistoryInternational.length > 0) ||
      (travelHistoryLocal && travelHistoryLocal.length > 0) ||
      clusterILI;

    /**
     *
     * @param {String} item
     */
    const mildSymptomsChecker = item => mildSymptomArray.indexOf(item) >= 0;

    /**
     *
     * @param {String} item
     */
    const severeSymptomChecker = item => severeSymptomArray.indexOf(item) >= 0;

    const mildSymptoms = symptoms.length > 0 && symptoms.filter(mildSymptomsChecker).length > 0;
    const severeSymptoms = symptoms.length > 0 && symptoms.filter(severeSymptomChecker).length > 0;

    fetch(`${apiLocation}/recommendation`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    }).then(res => res.json()).then(o => {
      if (o.data.actions) {
        const actions = o.data.actions;

        if (!isExposed) {
          if (!mildSymptoms && !severeSymptoms) {

            router.navigate('tips');

          } else {

            if (clusterILI) {

              if (severeSymptoms) {

                router.navigate('recommendationPUISevere');

              } else if (mildSymptoms) {
                if (comorbidity || withElderly) {

                  router.navigate('recommendationPUIMUE');

                } else {

                  router.navigate('recommendationPUIHQUE');

                }
              }

            } else if (severeSymptoms) {

              router.navigate('recommendationPUISevere');

            } else {
              // refer to er
              router.navigate('healthWorker');
            }

          }
        } else {

          if (!mildSymptoms && !severeSymptoms) {

            router.navigate('recommendationPUM');

          } else {

            if (mildSymptoms && !severeSymptoms) {

              if (comorbidity || withElderly) {

                router.navigate('recommendationPUIMPE');

              } else {

                router.navigate('recommendationPUIHQPE');

              }

            } else if (severeSymptoms) {

              router.navigate('recommendationPUISevere');

            }

          }

        }


      } else {
        messages.toast('Nagkaproblema habang inaalam ang rekomendasyon');
        console.log(o.message);
      }
    });
  },
  location: {
    verify() {
      var loc = form.radio.getValue('location');
      if (!loc) {
        messages.toast(messages.form.required);
      } else {
        if (loc === 'true') {
          router.navigate('disclaimerOne');
        } else {
          router.navigate('locationNotSupported');
        }
      }
    }
  },
  symptoms: {
    fever() {
      var p = 'feverA';
      var ans = form.radio.getValue(p);
      if (!ans) {
        messages.toast(messages.form.required);
      } else {
        store.set('fever', ans);
        router.navigate('symptomsTwo');
      }
    },
    breathing_problem() {
      var p = 'breathing_problem';
      var ans = form.radio.getValue(p);
      if (!ans) {
        messages.toast(messages.form.required);
      } else {
        store.set(p, ans);
        router.navigate('symptomsThree');
      }
    },
    direct_contact() {
      var p = 'direct_contact';
      var ans = form.radio.getValue(p);
      if (!ans) {
        messages.toast(messages.form.required);
      } else {
        store.set(p, ans);
        router.navigate('symptomsFour');
      }
    },
    comorbidity() {
      var p = 'co-morbidity';
      var ans = form.radio.getValue(p);
      if (!ans) {
        messages.toast(messages.form.required);
      } else {
        store.set(p, ans);
        router.navigate('symptomsFive');
      }
    },
    clusterILI() {
      var p = 'clusterILI';
      var ans = form.radio.getValue(p);
      if (!ans) {
        messages.toast(messages.form.required);
      } else {
        store.set(p, ans);
        router.navigate('symptomsSix');
      }
    },
    severe_condition() {
      var p = 'severe_condition';
      var ans = form.radio.getValue(p);
      if (!ans) {
        messages.toast(messages.form.required);
      } else {
        store.set(p, ans);
        router.navigate('symptomsSeven');
      }
    },

    with_elderly() {
      var p = 'with-elderly';
      var ans = form.radio.getValue(p);
      if (!ans) {
        messages.toast(messages.form.required);
      } else {
        store.set(p, ans);
        router.navigate('symptomsEight');
      }
    },

    list() {
      var r = form.radio.getAllAsArray([
        'fever',
        'headache',
        'dry-cough',
        'myalgia-arthralgia',
        'fatigue',
        'chills',
        'sputum',
        'nausea',
        'sore-throat',
        'vomiting',
        'nasal-congestion',
        'diarrhea',
        'breathing-difficulty',
        'whistling-sound'
      ]);
      if(r.indexOf('fever') === -1 && store.get('fever') === 'fever') r.push('fever');
      store.setObject('symptoms', r);
      actions.recommend();
    }
  },

  travel: {
    async fetchCountries() {
      fetch('assets/dataset/countries.json').then(r => r.json()).then(res => {
        $('.countries').html('');
        $('.countries').html('<option disabled selected>'+messages.travel.country+'</option>');
        for (const value of Object.values(res)) {
          $('.countries').append(`<option value='${value}'>${value}</option>`);
        }
      });
    },
    async getCities() {
      fetch('assets/dataset/cities.json').then(r => r.json()).then(res => {
        window['cities'] = res;
      })
    },
    async getRegions() {
      return fetch('assets/dataset/regions.json').then(r => r.json());
    },
    save() {
      var r = form.getAll([
        'countriesOneMobile',
        'citiesOneMobile',
        'countriesOneDesktop',
        'citiesOneDesktop',

        'countriesTwoMobile',
        'citiesTwoMobile',
        'countriesTwoDesktop',
        'citiesTwoDesktop',

        'regionsOneMobile',
        'regionsTwoMobile',
        'regionsOneDesktop',
        'regionsTwoDesktop',
      ]);

      var countriesOne, citiesOne, regionsOne, regionsTwo, countriesTwo, citiesTwo;

      countriesOne = r.countriesOneMobile || r.countriesOneDesktop;
      citiesOne = r.citiesOneMobile || r.citiesOneDesktop;
      regionsOne = r.regionsOneMobile || r.regionsOneDesktop;
      countriesTwo = r.countriesTwoMobile || r.countriesTwoDesktop;
      citiesTwo = r.citiesTwoMobile || r.citiesTwoDesktop;
      regionsTwo = r.regionsTwoMobile || r.regionsTwoDesktop;

      let p = true;

      if (!countriesOne && !countriesTwo) {
        router.navigate('symptomsIntro');
      } else {

        let travelHistoryLocal = [];
        let travelHistoryInternational = [];

        if (countriesOne) {
          if (countriesOne === 'Philippines') {
            if (!citiesOne) {
              p = false;
              messages.toast(messages.form.required);
            } else if (!regionsOne) {
              p = false
              messages.toast(messages.form.required);
            } else {
              /* travelHistoryLocal.push({
                region: regionsOne,
                city: citiesOne
              });*/
              travelHistoryLocal.push(citiesOne);
            }
          } else {
            /*
            travelHistoryInternational.push({
              region: '',
              city: countriesOne
            });
            */
            travelHistoryInternational.push(countriesOne);
          }
        }

        if (countriesTwo) {
          if (countriesTwo === 'Philippines') {
            if (!citiesTwo) {
              p = false;
              messages.toast(messages.form.required);
            } else if (!regionsTwo) {
              p = false
              messages.toast(messages.form.required);
            } else {
              travelHistoryLocal.push(citiesTwo);
              /*
              travelHistoryLocal.push({
                region: regionsTwo,
                city: citiesTwo
              });*/
            }
          } else {
            travelHistoryInternational.push(countriesTwo);
            /*
            travelHistoryInternational.push({
              region: '',
              city: countriesTwo
            });*/
          }
        }

        if (p) {
          store.setObject('travelHistoryLocal', travelHistoryLocal);
          store.setObject('travelHistoryInternational', travelHistoryInternational);
          router.navigate('symptomsIntro');
        }
      }

    }
  },

  aboutYou: {
    saveOne() {
      var data = form.getAll([
        "age",
        "ageDesktop"
      ]);

      const age = data.age || data.ageDesktop

      let p = true;

      if (!age) {
        messages.toast(messages.form.required);
        p = false;
      }

      if (p) {
        var e = { age: age };
        sessionStorage.setItem("aboutYouOne", JSON.stringify(e));
        router.navigate("aboutYouTwo");
      }
    },

    saveTwo() {
      var gender = form.radio.getValue("genderAbout") || form.radio.getValue("genderAboutDesktop");

      let p = true;

      if (!gender) {
        messages.toast(messages.form.required);
        p = false;
      }

      if (p) {
        $('#gender').val(gender);
        $('#genderDesktop').val(gender);
        var e = { gender: gender, };
        sessionStorage.setItem("aboutYouTwo", JSON.stringify(e));
        router.navigate("aboutYouThree");
      }
    },

    saveThree() {
      var data = form.getAll([
        "barangayAbout",
        "barangayAboutDesktop"
      ]);

      var barangay = data.barangayAbout || data.barangayAboutDesktop;

      let p = true;

      if (!barangay) {
        messages.toast(messages.form.required);
        p = false;
      }

      if (p) {
        var e = { barangay: barangay, };
        sessionStorage.setItem("aboutYouThree", JSON.stringify(e));
        $("#barangay").val(barangay);
        $("#barangayDesktop").val(barangay);
        router.navigate("travelIntro");
      }
    },
  },

  details: {
    saveOne() {
      var m = form.getAll([
        'first_name',
        'middle_name',
        'last_name',
        'birth_date',
        'marital_status',
        'gender',
        'occupation'
      ]);
      var d = form.getAll([
        'first_nameDesktop',
        'middle_nameDesktop',
        'last_nameDesktop',
        'birth_dateDesktop',
        'marital_statusDesktop',
        'genderDesktop',
        'occupationDesktop'
      ]);

      const first_name = m.first_name || d.first_nameDesktop;
      const middle_name = m.middle_name || d.middle_nameDesktop;
      const last_name = m.last_name || d.last_nameDesktop;
      const birth_date = m.birth_date || d.birth_dateDesktop;
      const marital_status = m.marital_status || d.marital_statusDesktop;
      const gender = m.gender || d.genderDesktop;
      const occupation = m.occupation || d.occupationDesktop;

      let p = true;

      if (!first_name) {
        messages.toast(messages.required.fname);
        p = false;
      } else if (!middle_name) {
        messages.toast(messages.required.mname);
        p = false;
      } else if (!last_name) {
        messages.toast(messages.required.lname);
        p = false;
      } else if (!birth_date) {
        messages.toast(messages.required.bday);
        p = false;
      } else if (!marital_status) {
        messages.toast(messages.required.status);
        p = false;
      } else if (!gender) {
        messages.toast(messages.required.gender);
        p = false;
      } else if (!occupation) {
        messages.toast(messages.required.occupation);
        p = false;
      }

      if (p) {
        var e = {
          first_name: first_name,
          middle_name: middle_name,
          last_name: last_name,
          birth_date: birth_date,
          marital_status: marital_status,
          gender: gender,
          occupation: occupation
        };
        sessionStorage.setItem('detailsOne', JSON.stringify(e));
        router.navigate('detailsTwo');
      }
    },
    saveTwo() {
      var m = form.getAll(['street_no', 'street', 'city', 'barangay', 'province', 'email', 'home_num', 'mobile_num']);
      var d = form.getAll(['street_noDesktop', 'streetDesktop', 'cityDesktop', 'barangayDesktop', 'provinceDesktop', 'emailDesktop', 'home_numDesktop', 'mobile_numDesktop']);

      const street_no = m.street_no || d.street_noDesktop;
      const street = m.street || d.streetDesktop;
      const city = m.city || d.cityDesktop;
      const barangay = m.barangay || d.barangayDesktop;
      const province = m.province || d.provinceDesktop;
      const email = m.email || d.emailDesktop;
      const home_num = m.home_num || d.home_numDesktop;
      const mobile_num = m.mobile_num || d.mobile_numDesktop;

      var p = true;

      if (!street) {
        messages.toast('Kailangan ang Street ');
        p = false;
      } else if (!city) {
        messages.toast('Kailangan ang City o Siyudad');
        p = false;
      } else if (!barangay) {
        messages.toast('Kailangan ang Barangay');
        p = false;
      } else if (!province) {
        messages.toast('Kailangan ang iyong probinsya');
        p = false;
      } else if (!email) {
        messages.toast('Kailangan ang iyong e-mail');
        p = false;
      } else if (!mobile_num) {
        messages.toast('Kailangan ang mobile number');
        p = false;
      } else if (!mobile_num) {
        messages.toast('Kailangan ang mobile number');
        p = false;
      }

      if (p) {
        var e = {
          street_no: street_no,
          street: street,
          city: city,
          barangay: barangay,
          province: province,
          email: email,
          home_num: home_num,
          mobile_num: mobile_num
        };
        sessionStorage.setItem('detailsTwo', JSON.stringify(e));
        actions.patient.saveInfo();
      }
    }
  },
  patient: {
    saveInfo() {
      function _calculateAge(birthdate) {
        var ageDifMs = Date.now() - new Date(birthdate).getTime();
        var ageDate = new Date(ageDifMs);
        return Math.abs(ageDate.getUTCFullYear() - 1970);
      }

      const detailsOne = store.getObject('detailsOne');
      const detailsTwo = store.getObject('detailsTwo');
      const symptoms = store.getObject('symptoms');
      const travelHistoryLocal = store.getObject('travelHistoryLocal');
      const travelHistoryInternational = store.getObject('travelHistoryInternational');
      const exposure = store.getObject('direct_contact');
      const withElderly = store.getObject('with-elderly');
      const clusterILI = store.getObject('clusterILI');
      const comorbidity = store.getObject('co-morbidity');
      const severeCondition = store.get('severe_condition');
      if (symptoms == [] || symptoms.length > 0) {
        if (severeCondition == 'true') symptoms.push('severe-condition');
      }

      var info = [{
        firstname: detailsOne.first_name,
        middlename: detailsOne.middle_name,
        lastname: detailsOne.last_name,
        status: detailsOne.marital_status,
        gender: detailsOne.gender,
        email: detailsTwo.email,
        occupation: detailsOne.occupation,
        address: `${detailsTwo.street_no}, ${detailsTwo.street}`,
        barangay: detailsTwo.barangay,
        province: detailsTwo.province,
        city: detailsTwo.city,
        birthdate: detailsOne.birth_date,
        age: _calculateAge(detailsOne.birth_date),
        homenumber: detailsTwo.home_num,
        mobilenumber: detailsTwo.mobile_num,
        symptoms: symptoms,
        travelHistoryInternational: travelHistoryInternational || [],
        travelHistoryLocal: travelHistoryLocal || [],
        exposure: exposure,
        clusterILI: clusterILI,
        comorbitities: comorbidity,
        withElderly: withElderly
      }];

      fetch(`${apiLocation}/patient`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(info)
        })
        .then(o => o.json())
        .then(res => {
          if (res.success) {
            router.navigate('done');
          } else {
            messages.toast('Nagkaproblema sa pagsend ng datos');
          }
        }).catch(e => {
          messages.toast('Nagkaproblema sa pagsend ng datos');
          console.log(e);
        });
    }
  }
};

function changeLanguage()
{
  if(lang == "fil")
  {
    lang = "en"
  }
  else
  {
    lang = "fil";
  }

  mapMessages();
}

async function loadMessages(){
  // let msgUrl = "/public/messages/messages.json";
  let msgUrl = "/messages/messages.json";
  messagesObject = await fetch(msgUrl).then(res => res.json());
  mapMessages();
}

function mapMessages()
{
  // console.log("messagesObject", messagesObject);
  for(let key in messagesObject)
  {
    if(messagesObject.hasOwnProperty(key))
    {
      $(`msg#${key}`).html(messagesObject[key][lang]);
    }
  }

  //REQUIRED
  messages.form.required = messagesObject['MSG_GEN_REQUIRED'][lang];
  messages.form.requiredAll =  messagesObject['MSG_GEN_REQUIRED_ALL'][lang];
  //TRAVEL
  messages.travel.country = messagesObject['MSG_GEN_SELECT_COUNTRY'][lang];
  messages.travel.city = messagesObject['MSG_GEN_SELECT_CITY'][lang];
  $("select.countries").each(function(){
    $(this).children("option:first").html(messages.travel.country);
  });
  $("select.cities").each(function(){
    $(this).children("option:first").html(messages.travel.city);
  });
  //GENDER SELECTION
  $("select.gender-selection").each(function(){
    var childCount = 1;
    $(this).children("option").each(function(){
      switch(childCount)
      {
        case 1:
          $(this).html(messagesObject['MSG_PR_GENDER'][lang]);
          break;
        case 2:
          $(this).html(messagesObject['MSG_PR_GENDER_M'][lang]);
          break;
        case 3:
          $(this).html(messagesObject['MSG_PR_GENDER_F'][lang]);
          break;
      }
      childCount ++;
    });
  });
  //INFO
  messages.info.loadingResult = messagesObject['MSG_GEN_LOADING_RESULT'][lang];
  //FORM
  for(let formKey in messages.required)
  {
    if(messages.required.hasOwnProperty(formKey))
    {
      let template = "";
      if(lang == "en")
      {
        template = messagesObject[`MSG_PR_${formKey.toUpperCase()}`][lang] + " is required";
      }
      else
      {
        template = "Kailangan ang " + messagesObject[`MSG_PR_${formKey.toUpperCase()}`][lang];
      }
      messages.required[formKey] = template;
    }
  }
}

function loadHotlines()
{
  var hotlines = [
    {
      label : "Naga COVID-19 Hotline (Call Only)",
      number : "0917-154-3158"
    },
    {
      label : "Naga COVID-19 Hotline (Text and Call)",
      number : "0998-435-2138"
    },
    {
      label : "Medical Emergency Hotlines (Text and Call)",
      number : "0908-885-3000"
    },
    {
      label : "City Hall Trunk Line",
      number : "054-205-2980"
    },
    {
      label : "DOH Hotline",
      number : "(02)984-COVID"
    },
    {
      label : "DOH Hotline for PLDT, SMART, SUN and TNT subscribers:",
      number : "1555"
    }
  ];

  var hotlineString = "";

  for(let ctr=0; ctr < hotlines.length; ctr++)
  {
    let item = hotlines[ctr];
    hotlineString += `
      <p>${item.label}</p>
      <p class="hotline-holder">
        <img src="assets/images/phone.svg" alt="Phone Icon" loading="lazy">
        <span class="phone-number">${item.number}</span>
      </p>
      `;
  }

  $(".hotlines").html(hotlineString);
}
