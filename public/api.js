/* eslint-disable no-return-await */
// @ts-ignore
window.API = {
  patient: {
    url: 'https://backend-serverless-vkupy7dkca-ue.a.run.app/api/patient',
    async getOne (pid) {
      return await fetch(`${this.url}/${pid}`, { method: 'GET' }).then(r => r.json());
    },
    async putOne (pid, body) {
      return await fetch(`${this.url}/${pid}`, { method: 'PUT', 'Content-Type': 'application/json', body: JSON.stringify(body) }).then(r => r.json());
    },
    async deleteOne (pid) {
      return await fetch(`${this.url}/${pid}`, { method: 'DELETE' }).then(r => r.json());
    },
    async post (obj) {
      return await fetch(`${this.url}`, { method: 'POST', 'Content-Type': 'application/json', body: JSON.stringify([obj]) }).then(r => r.json());
    },
    async get (object) {
      return await fetch(`${this.url}/?limit=${object.limit || ''}&offset=${object.offset || '0'}&filter=${object.filter || ''}`, { method: 'GET' }).then(r => r.json());
    },
    async put (array) {
      return await fetch(`${this.url}`, { method: 'PUT', 'Content-Type': 'application/json', body: array }).then(r => r.json());
    },
    async delete (array) {
      return await fetch(`${this.url}`, { method: 'DELETE', 'Content-Type': 'application/json', body: array }).then(r => r.json());
    }
  }
};
