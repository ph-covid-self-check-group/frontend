module.exports = {
  "globDirectory": "public/",
  "globPatterns": [
    "**/*.{js,png,css,json,svg,jpg,ico,html,txt}"
  ],
  "swDest": "public/sw.js"
};