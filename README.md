# PH Covid Selfcheck Frontend

This is the frontend version of the system

# Installation

You should have the following:
- Node version 10 or 12
- Any lightweight http server

To install testing & linting dependencies run,
```bash
npm install
```

# Usage

To run this project, run a web server with the `public` directory as the entrypoint.

## PHP
```bash
php -S localhost:8080
```
## Python
```
python -m SimpleHTTPServer 8080
```

# Code Organization
This is was originally built as a prototype hence leveraging only jquery for some parts, otherwise the entirety is in vanilla javascript.

## public/index.html
This file contains the pages/views. The pages are in `<section>` and each are identified using `id` and must include the word Activity at the end.

Sample page/view:
```html
<section id="sampleActivity">
  <h1>Hello World</h1>
</section>
```

Sample router call to show sampleActivity
```javascript
router.navigate('sample'); // sampleActivity shows.
```

### Notes!
- Being a single file this could get really long! In order to navigate easily across pages, fold the code using your IDE to every `<section>` to easily glance through each page.

 - The site follows a PRPL pattern for a better initial-load performance, only the important styles are at the top and anything uncessary are at the bottom. For more info visit https://web.dev/apply-instant-loading-with-prpl/

## public/app.js
This file contains the entirety of the functions of the web app.

### `store` object
The store object contains the methods necessary for manipulating sessionStorage.

```javascript
const store = {
  get(key) {}, // for getting a string
  getObject(key) {}, // for getting an object (passes through JSON.parse())
  set(key, value) {}, // for storing a string
  setObject(key, value) {} // for storing an object (passes through JSON.stringify())
};
```

### `utils` object
The utils object contains the methods for string, object manipulation.

```javascript
const utils = {
  array: {
    contains(needle, haystack) {} // high-level method for checking if 'needle'(string) is in a 'haystack'(array);
  }
};
```

### `messages` object
The messages object contains the methods for modals and toasts. Note: the toast contents should be moved at a later time to utilize an internalization library.

```javascript
const messages = {
  toast(message) {} // shows a toast to the user. High-level abstraction of M.toast(obj);
};
```

### `router` object
The router object handles the routing mechanism of this PWA. Use this in navigating to different pages/views/activity.

```javascript
const router = {
  clear() {}, // clears the current activity (not needed if using this.navigate method)
  navigate(id) {}, // navigates the user to a certain page/view
  handle() {} // routing logic (used only by document.ready & event listener for hash change)
};
```

It is an important reminder that when creating views/activities to use the word `Activity` at the end of the id. Also we `<section>` for each view/activity/page. Using it on other parts can result in unknown consequences (might not show up).

Ex.: To show the home activity do the following:

```html
<section id="homeActivity"></section>
```

```javascript
router.navigate('home'); // redirects to a section with .homeActivity id
```

### `form` object
The form object contains the high-level methods for form manipulation. This abstracts jquery so that it can either be kept, replaced or removed in the future. This object keeps the code clean and form manipulation faster.

```javascript
const form = {
  getValue(id) {}, // gets the value of any input (text, password, email, number, tel) using an id.
  getAll(list) {}, // same as getValue but for multiple inputs. (Accepts array) RETURNS OBJECT!
  radio: {
    getValue(name) {}, // gets the value of a radio input
    getAll(list) {}, // same as getValue but for multiple radio inputs. (Acceps array) RETURNS OBJECT
    getAllAsArray(list) {} // same as getAll but RETURNS ARRAY
  }
};
```

### `actions` object
Finally, the actions object contains the user-defined methods. Feel free to add methods to this object according to the requirements of the project.

```javascript
const actions = {
  // insert objects or methods here
};
```

In calling methods from the view/activity/page, use the vanilla js methods ex: `onclick`.
Ex.:
```html
<button onclick="actions.example.sayHello()">Say hello</button>
```

```javascript
const actions = {
  example: {
    sayHello() {
      console.log('Hello World');
    }
  }
};
```

## public/api.js
This file contains the API methods in communicating with the backend.

```javascript
window.API = {
  patient: {
    url: '', // url of the API
    async getOne (pid) {}, // gets one patient object using patientId
    async putOne (pid, body) {}, // puts one patient object (requires patientId and patient object)
    async deleteOne (pid) {}, // deletes one patient object using patientId
    async post (obj) {}, // updates array of patient objects
    async get (object) {}, // gets array of patients, use a patientPointer in navigating ({limit, offset, filter});
    async put (array) {}, // updates array of patient object
    async delete (array) {} // deletes array of patient object
  }
};
```

### Sample Usage
```javascript

function getPatient() {
  const patientId = 'patient123';

  window.API.patient.getOne(patientId).then(res => {
    if(res.success) {
          const patientObject = res.data;
          console.log(patientObject);
    } else {
      console.log(res.message);
    }
  }).catch(e => {
    console.error(e);
  });

}

```

## public/serviceworker.js
This file contains the service worker script. This contains the version code and the list of files that needs to be cached in the client-side.

You must raise the version number to trigger an update on the client-side.

```javascript
let cn = 'version1'; // the current version number
let cacheWhiteList = ['version1']; // contains the versions to keep cache, everything not here are removed.
let assetsList = []; // contains the list of files to cache
```

# Documentation

Please visit the repo page or if you want to build your own copy here, you have to install Hugo globally

```bash
brew install hugo
```

Then run the following commands:

```bash
npm run serve-docs
```

If you want to just build the docs,

```bash
npm run build-docs
```
